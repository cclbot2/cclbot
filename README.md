# cclbot

When users post a link to a Reddit comment, cclbot automatically comments with links with various levels of ?context.

Definitions: The *referring item* (post or comment) refers to the *referred item* (comment or post).

Requirements:
* Must only interact with specified subreddits.
* Must only create comments.
* Must not comment more than once per referring item.
* Must clearly state how to report issues.
* Should not comment if the referenced item is not a comment. (Context is never needed for referenced posts.)
* Should not comment if the referenced item is a top-level comment (has no parents).
* Should not comment if the referring item already specifies ?context.

Currently this bot can only scan and reply to referring posts, but it could be extended to referring comments in the future.
