import praw
import configparser
import sys
import urllib.parse as urlparse
import os
import time

LAST_READ_FILEPATH = "/home/pi/.local/var/cclbot-last-read-post.txt"

def main(reddit,
         dryrun = False,
         subreddit_name = 'bestof+DepthHub',
         last_read_post_filepath = "/usr/var/cclbot-last-read-post.txt",
         hours_to_run = 0.9,
         active_pause_seconds = 10,
         inactive_pause_seconds = 600,
         comment_cooldown_seconds = 600):

	starttime = time.time()
	subreddit = reddit.subreddit(subreddit_name)

	for i_batch in range(1000):

		assert time.time() < starttime + hours_to_run * 3600
		last_read_post = determine_last_read_post(reddit, subreddit)
		params = dict(before = last_read_post, limit = 100)
		posts = list(subreddit.new(params = params))
		# process oldest first so that "last read" can indicate
		# which posts to read next
		posts.reverse()
		for referrer in posts:

			# check time limit here to minimize risk of overlap with
			# next process
			assert time.time() < starttime + hours_to_run * 3600

			print("#" * 79)
			with open(LAST_READ_FILEPATH, 'w') as f:
				f.write(referrer.fullname)

			print(f"reddit.com/r/{referrer.subreddit.display_name}/comments/{referrer.id}")
			print(time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime(referrer.created_utc)))
			print(f"Refers to {referrer.url}")

			try:
				comment_text = create_comment(reddit, referrer)
			except Exception as e:
				print(str(e) + ' Skipping.')
				continue
			print(comment_text)
			if not dryrun:
				referrer.reply(comment_text)
				print(f'Pausing {comment_cooldown_seconds} seconds after commenting...')
				time.sleep(comment_cooldown_seconds)

		# pause a bit before requesting another batch
		pause_seconds = active_pause_seconds if len(posts) > 0 else inactive_pause_seconds
		print(f'Pausing {pause_seconds} before next batch...')
		time.sleep(pause_seconds)


def determine_last_read_post(reddit, subreddit):
	try:
		with open(LAST_READ_FILEPATH) as f:
			last_read_post = f.read().strip()
	except FileNotFoundError:
		last_read_post = 't3_gp39ex'

	# If the last read post has been removed then
	# subreddit.new will return no results --> use alternative.
	submission = reddit.submission(last_read_post[3:])
	if submission.author is None or submission.removed_by_category is not None:
		deleted_post = last_read_post
		print(f'Last read post {deleted_post} appears to be removed. Considering alternatives...')
		newest_first = [post.fullname for post in subreddit.new(params = dict(limit = 1000))]
		newest_first.sort(reverse=True)  # ensure newest is first
		for post_fullname in newest_first:
			if post_fullname < deleted_post:
				print(f"Falling back to older post {post_fullname}.")
				last_read_post = post_fullname
				break
		else:
			print(f"Unable to find older post in latest 1000, so defaulting to {newest_first[-1]}.")
			last_read_post = newest_first[-1]

	return(last_read_post)


def create_comment(reddit, referrer):

	url = referrer.url

	# parse the url and its query
	parsed = urlparse.urlparse(url)
	query_dict = dict(urlparse.parse_qsl(parsed.query))
	assert 'context' not in query_dict, \
		"Referrer appears to already have provided context."

	# count the number of ancestor comments (excluding the post and
	# referred comment)
	try:
		referred_comment = reddit.comment(url = url)
	except praw.exceptions.InvalidURL:
		raise Exception("Referred URL appears to not be a comment.")
	n_ancestors = 0
	ancestor = referred_comment
	for i in range(1000):
		ancestor = ancestor.parent()
		if type(ancestor) is not praw.models.reddit.comment.Comment:
			break
		n_ancestors += 1
	assert n_ancestors > 0, \
		"Referred comment appears to be a top-level comment, so no context is needed."

	# check that this bot has never posted a comment on this post
	# redundant with other checks, but redundancy doesn't hurt
	for comment in referrer.comments:
		try: author_name = comment.author.name
		except AttributeError: continue
		assert author_name != reddit.user.me().name, \
			"It appears that I've already commented on this."

	# format the new comment
	parsed = list(parsed)  # ParseResult is read-only
	urls = list()
	labels = list()
	for i in range(n_ancestors, -1, -1):
		query_dict['context'] = str(i)
		parsed[4] = urlparse.urlencode(query_dict)  # edit the "query"
		urls.append(urlparse.urlunparse(parsed))
		labels.append(f"Show {i} of {n_ancestors} parent comment{'s' if n_ancestors > 1 else ''}")
	labels[-1] = 'Show only the linked comment (no parents)'
	url_items = "\n".join(f"* [{label}]({url})"
	                      for (label, url)
	                      in zip(labels, urls))

	return ("It may be difficult for some users to see the parent comments that led to the linked comment. "
	        "These links provide context:"
	        f"\n\n{url_items}\n\n"
	        "I am a Comment Context Linker Bot, currently being tested. "
	        #"Information and the source code is available at gitlab.com/cclbot2/cclbot. "
	        f"Please contact /u/{reddit.user.me().name} if you have any questions or concerns.")


if __name__ == '__main__':
	ini_paths = [arg for arg in sys.argv if arg.endswith('.ini')]
	assert len(ini_paths) == 1, 'An ini path must be provided on the command line.'
	config = configparser.ConfigParser()
	config.read(ini_paths[0])
	reddit = praw.Reddit(**dict(config['connection']))
	config_main = config['main']

	main(reddit,
	     dryrun = config_main.getboolean('dryrun'),
         subreddit_name = config_main['subreddit_name'],
         last_read_post_filepath = config_main['last_read_post_filepath'],
         hours_to_run = config_main.getfloat('hours_to_run'),
         active_pause_seconds = config_main.getfloat('active_pause_seconds'),
         inactive_pause_seconds = config_main.getfloat('inactive_pause_seconds'),
         comment_cooldown_seconds = config_main.getfloat('comment_cooldown_seconds'))
